import React from 'react';

// Creates a context object
	// Context object as the name states is a data type of an objkect that can be used to store information that can be shared to other components within the app.
	// The context object is a diferent approach to passing information between components and allows us for easier access by avoiding the use of prop drilling

const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;