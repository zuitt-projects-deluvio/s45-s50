import {Row, Col, Card, Button} from 'react-bootstrap';
import {useState} from 'react';
import {Link} from 'react-router-dom';
import Courses from '../data/coursesData';

export default function CourseCard ({courseData}) {

	const {name, description, price, _id} = courseData;


	// const [count, setCount] = useState(0);
	
	// const [seat, setSeat] = useState(30);
    
 //    function enroll(){
 //    	if(seat === 0){
 //    		return alert("There are no seats left")

 //    	} else {
 //    		setCount(count + 1);
 //        	setSeat(seat - 1)
 //    	}
        
 //    };

	return (


		<Row className="mt-3 mb-3">
			<Col xs={12} md={12}>
				<Card className="cardHighlight p-3">
					
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>

						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{`PhP ${price}`}</Card.Text>
						
						<Button variant="primary" as={Link} to={`/courses/${_id}`}>
							See details
						</Button>

					</Card.Body>
				</Card>
			</Col>
		</Row>

		// <Row className="mt-3 mb-3">
		// 	{courseData.slice(0,3).map(entry => {

		// 		return (
		// 			<Col xs={12} md={4}>
		// 				<Card className="cardHighlight p-3">
							
		// 					<Card.Body>
		// 						<Card.Title>{entry.name}</Card.Title>
		// 						<Card.Subtitle>{entry.description}</Card.Subtitle>

		// 						<Card.Subtitle>Price</Card.Subtitle>
		// 						<Card.Text>{`PhP ${entry.price}`}</Card.Text>
		// 						<Button variant="primary">Enroll</Button>
		// 					</Card.Body>
		// 				</Card>
		// 			</Col>
		// 		)
		// 	})}
		// </Row>


		

	)
}

