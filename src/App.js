import {Fragment, useState, useEffect} from 'react';
import './App.css';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import CourseView from './components/CourseView';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Courses from './pages/Courses';
import LogIn from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import {UserProvider} from './UserContext'




function App() {

    const [user, setUser] = useState({email: localStorage.getItem('email')})
    console.log(user)
    const unsetUser = () => {
        localStorage.clear()
    }

    useEffect(() => {
        fetch('http://localhost:4000/users/details', {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data._id !== "undefined"){
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            } else {
                setUser({
                    id: null,
                    isAdmin: null
                })
            }
        })
    }, [])


    return ( 

    <UserProvider value={{user, setUser, unsetUser}}>


        <Router>

            <AppNavbar />

            <Container>

                <Routes>

                    <Route exact path="/" element={<Home />}/>
                    <Route exact path="/courses" element= {<Courses />}/>
                    <Route exact path="/courses/:courseId" element= {<CourseView />}/>

                    <Route exact path="/register" element= {<Register />}/>
                    <Route exact path="/login" element= {<LogIn />}/>
                    <Route exact path="/logout" element= {<Logout />}/>
                    <Route path="*" element={<Error />} />

                </Routes>

            </Container>

        </Router>  

    </UserProvider>
    )
}

export default App;
