const coursesData = [

	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Earum doloribus sed, ratione ex deserunt delectus voluptatibus ducimus repellendus perferendis commodi unde itaque quis omnis a labore, officiis quidem vero amet!",
		price: 45000,
		onOffer: true
	},

	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Earum doloribus sed, ratione ex deserunt delectus voluptatibus ducimus repellendus perferendis commodi unde itaque quis omnis a labore, officiis quidem vero amet!",
		price: 50000,
		onOffer: true
	},

	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing, elit. Earum doloribus sed, ratione ex deserunt delectus voluptatibus ducimus repellendus perferendis commodi unde itaque quis omnis a labore, officiis quidem vero amet!",
		price: 60000,
		onOffer: true
	},

]

export default coursesData;