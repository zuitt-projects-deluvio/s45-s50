import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';
import {useEffect, useState} from 'react';

export default function Courses() {

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/courses')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {

				return (
					<CourseCard key={course._id} courseData={course}/>
				);
			}));
		})
	}, [])

	return (
		<>
			<h1 className="my-4">Courses</h1>
			{courses}
		</>
	)

}