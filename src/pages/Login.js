import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext'
import {Navigate} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function LogIn() {

	const {user, setUser} = useContext(UserContext);
	// console.log(user);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

	function authenticate(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.access)
			
			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
			} else {
				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Please check your credentials'
				})
			}
		})

		// localStorage.setItem("email", email);

		

		// to access the user information it can be done using localStorage, this is necessary to update the user state which will help update the app component and rerender to avoid manually refreshing the page upon use login and logout

		//when state change componenets are rerendered and the AppNavBar component will be updated based on the user credentials.
		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		setEmail('');
		setPassword('');

		const retrieveUserDetails = (token) => {
			fetch('http://localhost:4000/users/details', 
			{
				method: "POST",
				headers: {
					Authorization : `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
			
				setUser({
						id: data._id,
						isAdmin: data.isAdmin
				})
			})
		}
	};

	useEffect(() => {

		if(email !== '' && password !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email, password]);

	return (

		(user.id !== null) ?
		<Navigate to="/courses"/>
		:
		<>
		<Form className="my-4" onSubmit={e => authenticate(e)}>
			
			<h1>LogIn</h1> 
			<Form.Group className="mt-3" controlId="userEmail">
				<Form.Label>Email Address</Form.Label>

				<Form.Control
					type = "email"
					placeholder = "Enter your email here"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>

			</Form.Group>

			<Form.Group className="mt-3" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type = "password"
					placeholder = "Input your password here"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive ?
				<Button 
				variant="success" 
				type="submit" 
				id="submitBtn"
				className="mt-3"
				>
				Submit
				</Button>
				:
				<Button 
				variant="success" 
				type="submit" 
				id="submitBtn"
				className="mt-3 disabled"
				>
				Submit
				</Button>
			 }
		</Form>
		</>

	)
}